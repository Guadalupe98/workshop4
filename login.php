<?php
  require('funciones.php');

  if($_POST) {
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];
    
    $user = authenticate($username, $password);

    if($user) {
        if ($user["tipousu"] == 1) {

            $_SESSION['persona'] = $user;
            header("Location:inicioAdmin.php");            
        }else if ($user["tipousu"] == 2) {
            printf ("Bienvenido  %s %s\n", $user["nombre"], $user["apellido"]);
            $_SESSION['persona'] = $user;
        }
      
    }else{
      header('Location: /index.php?status=login');
    }
  }
?>